//
//  MarvelAPI.swift
//  HeroisMarvel
//
//  Created by Wagner Rodrigues on 09/02/2018.
//  Copyright © 2018 Eric Brito. All rights reserved.
//

import Foundation
import SwiftHash
import Alamofire

class MarvelAPI {
    
    static private let basePath = "https://gateway.marvel.com/v1/public/characters?"
    static private let privateKey = "c4a4e1fa0bd93501cafe74d210459e8e3354b3fa"
    static private let publicKey = "47b5a3fd6970d45242dd11533de4c5cd"
    static private let limit = 50
    
    class func loadHero(name: String?, page: Int = 0, onComplete: @escaping (MarvelInfo?) -> Void) {
        let offset = page * limit
        let startWith : String
        if let name = name, !name.isEmpty {
            startWith = "nameStartsWith=\(name.replacingOccurrences(of: " ", with: ""))&"
        } else {
            startWith = ""
        }
        let url = basePath + "offset=\(offset)&limit=\(limit)&" + startWith + getCredencials()
        print(url)
        
        Alamofire.request(url).responseJSON { (response) in
            guard let data = response.data else {
                onComplete(nil)
                return
            }
            do{
                let marvelInfo = try JSONDecoder().decode(MarvelInfo.self, from: data)
                    onComplete(marvelInfo)
            }catch{
                print(error.localizedDescription)
                onComplete(nil)
            }
          
        }
    }
    
    private class func getCredencials() -> String {
        let ts = String(Date().timeIntervalSince1970)
        let hash = MD5(ts+privateKey+publicKey).lowercased()
        return "ts=\(ts)&apikey=\(publicKey)&hash=\(hash)"
    }
}
